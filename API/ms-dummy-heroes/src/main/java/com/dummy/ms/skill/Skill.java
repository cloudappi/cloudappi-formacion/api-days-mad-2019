package com.dummy.ms.skill;

import com.dummy.ms.hero.Hero;
import lombok.Data;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private String description;

    @ManyToMany(mappedBy = "skills")
    private List<Hero> heroes;
}