package com.dummy.ms.hero;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "heroes", path = "heroes")
public interface HeroRepository extends PagingAndSortingRepository<Hero, Long> {
    List<Hero> findByAlias(@Param("alias") String name);
}
