package com.dummy.ms.hero;

import com.dummy.ms.messaging.EmitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@RepositoryEventHandler
public class HeroEventHandler {

    Logger logger = Logger.getLogger("Class HeroEventHandler");
    @Autowired
    private EmitterService emitterService;

    @HandleBeforeCreate
    public void handleAuthorBeforeCreate(Hero hero){
        emitterService.sendMessage("Creating Hero: " + hero.toString());

    }
}
