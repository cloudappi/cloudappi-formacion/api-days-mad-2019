package com.dummy.ms.messaging;

import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;

@EnableKafka
@Configuration
public class Consumer {
    @KafkaListener(topics = "asgard")
    public void listen(String message) {
        System.out.println("CONSUMER: Received Message in group foo: " + message);
    }
}
