package com.dummy.ms.skill;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "skills", path = "skills")
public interface SkillRepository extends PagingAndSortingRepository<Skill, Long> {
    List<Skill> findByName(@Param("name") String name);
}
