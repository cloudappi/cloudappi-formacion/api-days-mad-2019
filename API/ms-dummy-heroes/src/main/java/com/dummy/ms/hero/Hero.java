package com.dummy.ms.hero;

import com.dummy.ms.skill.Skill;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Hero {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private String surname;
    private String alias;
    private String weakness;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "skill_hero",
            joinColumns = @JoinColumn(name = "skill_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "hero_id",
                    referencedColumnName = "id"))
    private List<Skill> skills;
}